package fly.force.com.DB;

public interface FlyDbI {
    int randFuel();
    int randCapacity();
    int randRoomines();
    int randDistance();
}
