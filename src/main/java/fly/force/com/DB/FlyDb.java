package fly.force.com.DB;

import java.util.Random;

/*random data class creation*/
public class FlyDb implements FlyDbI{

    @Override
    public int randFuel(){
        Random random = new Random();
        int fuel = random.nextInt(300) + 100;
        return fuel;
    }

    @Override
    public int randCapacity(){
        Random random = new Random();
        int capacity = random.nextInt(220) + 60;
        return capacity;
    }

    @Override
    public int randRoomines(){
        Random random = new Random();
        int roomines = random.nextInt(400) + 80;
        return roomines;
    }

    @Override
    public int randDistance(){
        Random random = new Random();
        int distance = random.nextInt(3400) + 200;
        return distance;
    }
}
