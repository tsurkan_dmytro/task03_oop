package fly.force.com.Entity;

import java.util.Random;

public enum ItemFlyType {
    BOING_737,
    BOING_757,
    AIRBUS_300,
    AN_225,
    AN_22,
    AN_74,
    IL_76;

    public static ItemFlyType getFlType() {
        Random random = new Random();
        return values()[random.nextInt(values().length)];
    }
}