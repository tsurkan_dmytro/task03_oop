package fly.force.com.Builders;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class MenuBuilderView {

    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input = new Scanner(System.in);

    ComanyBuilderModel comanyBuilderModel = new ComanyBuilderModel();

    public MenuBuilderView() {
        mainManu();
    }

    public void mainManu(){

        menu = new LinkedHashMap<>();
        menu.put("1", "  1 - Show all flights");
        menu.put("2", "  2 - Show by distance");
        menu.put("3", "  3 - Find avia by fuel");
        menu.put("Q", "  Q - exit");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::showAllData);
        methodsMenu.put("2", this::sortByDistance);
        methodsMenu.put("3", this::findByPower);
    }

    public void powerEnter(){
        System.out.println("Enter your fuel count:");
    }

    public void showAllData(){
        comanyBuilderModel.showAllData();
    }

    public void sortByDistance(){
        comanyBuilderModel.sortByDistance();
    }

    public void findByPower(){
        powerEnter();
        int pos = 0;
        pos = menuTotalPowerEnter();
        comanyBuilderModel.findByPower(pos);
    }

    public int menuTotalPowerEnter(){
        int visitorPrice = 0;

        try {
            while (visitorPrice <= 0 ){
                try {
                    BufferedReader inPrice = new BufferedReader(new InputStreamReader(System.in));
                    visitorPrice = Integer.parseInt(inPrice.readLine());
                }catch (NumberFormatException c){}
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return visitorPrice;
    }
    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }
}
