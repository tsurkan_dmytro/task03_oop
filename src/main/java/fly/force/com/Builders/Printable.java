package fly.force.com.Builders;

@FunctionalInterface
public interface Printable {

    void print();
}
